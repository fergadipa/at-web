/* eslint no-unused-vars: off */
/* global env */
const nightwatchCucumber = require('nightwatch-cucumber');
const chromedriverPath = require('chromedriver').path;
const seleniumServerPath = require('selenium-server').path;
const env = require('dotenv').config();

nightwatchCucumber({
  nightwatchOutput: true,
  cucumberArgs: [
    '--require', 'tests/hooks.js',
    '--require', 'tests/steps',
    '--format', 'json:reports/cucumber.json',
    '--format', 'node_modules/cucumber-pretty',
    'tests/features',
  ],
});

module.exports = {
  output_folder: 'reports',
  page_objects_path: 'tests/pages',
  custom_assertions_path: '',
  live_output: false,
  disable_colors: false,
  selenium: {
    start_process: true,
    server_path: seleniumServerPath,
    cli_args: {
      'webdriver.chrome.driver': chromedriverPath,
    },
    host: '127.0.0.1',
    port: 4444,
  },
  test_settings: {
    default: {
      globals: {
        url: process.env.PRODUCTION_WEB_BASE_URL,
      },
      screenshots: {
        enabled: true,
        on_failure: true,
        path: 'screenshots',
      },
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        chromeOptions: {
          args: [
            'start-maximized',
          ],
        },
        acceptSslCerts: true,
      },
    },
  },
};
