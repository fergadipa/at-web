const { client } = require('nightwatch-cucumber');

const authentication = {
    elements: {
        txtEmail : '#email',
        txtPassword : '#passwd',
        btnSignIn : '#SubmitLogin',
        txtEmailCreateAccount : '#email_create',
        btnCreateAccount : '#SubmitCreate'
    },
    commands: [{
        async inputEmail(textEmail) {
            await client.waitForElementVisible(authentication.elements.txtEmail,5000)
            await client.setValue(authentication.elements.txtEmail,textEmail);
        },
        async inputPassword(password) {
            await client.waitForElementVisible(authentication.elements.txtPassword, 5000);
            await client.setValue(authentication.elements.txtPassword, password);
        },
        async clickSubmitLogin() {
            await client.waitForElementVisible(authentication.elements.btnSignIn, 5000);
            await client.click(authentication.elements.btnSignIn);
        },
        async inputEmailCreateAccount(textEmailCreateAccount){
            await client.waitForElementVisible(authentication.elements.txtEmailCreateAccount, 5000);
            await client.setValue(authentication.elements.txtEmailCreateAccount, textEmailCreateAccount);
        },
        async clickSubmitCreateAccount() {
            await client.waitForElementVisible(authentication.elements.btnCreateAccount, 5000);
            await client.click(authentication.elements.btnCreateAccount);
        }
    }]
}
module.exports = authentication;