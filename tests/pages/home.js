const { client } = require('nightwatch-cucumber');

const home = {
    elements: {
        fieldType : '.newsletter-input',
        btnSubmit : '.form-group .btn',
        alert : '.alert-success',
        btnSignIn : '.login',
    },
    commands: [{
        async clickSignIn() {
            await client.waitForElementVisible(home.elements.btnSignIn, 5000);
            await client.click(home.elements.btnSignIn);
        },
        async inputEmail(email) {
            await client.waitForElementVisible(home.elements.fieldType, 5000)
            await client.click(home.elements.fieldType)
            await client.setValue(home.elements.fieldType, email)
        },
        async clickSubmitButton(btnSubmit) {
            await client.waitForElementVisible(home.elements.btnSubmit, 5000)
            await client.click(home.elements.btnSubmit)
        },
        async verifyNewsletterNotification(alert) {
            await client.waitForElementVisible(home.elements.alert, 5000)
            await client.expect.elements(home.elements.alert)
        }
    }]
};

module.exports = home;