const {client} = require('nightwatch-cucumber');

const accountcreation = {
    elements : {
        accountCreationForm : '#account-creation_form'
    },
    commands : [{
        async accountcreateform(){
            await client.waitForElementVisible(accountcreation.elements.accountCreationForm, 15000);
        }
    }]
}

module.exports = accountcreation;