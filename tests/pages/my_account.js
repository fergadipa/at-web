const { client } = require('nightwatch-cucumber');

const myAccount = {
    elements : {
        labelName : '.account > span'
    },
    commands: [{
        async verificationName(nama) {
            await client.waitForElementVisible(myAccount.elements.labelName, 5000);
            await client.expect.element(myAccount.elements.labelName).text.to.equal(nama)
        }
    }]
};

module.exports = myAccount