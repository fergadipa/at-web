const { client } = require('nightwatch-cucumber');
const { Then } = require('cucumber');

const accountCreation = client.page.account_creation();

Then(/^I see account creation form$/, async() => {
    await accountCreation.accountcreateform();
});