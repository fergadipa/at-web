const { client } = require('nightwatch-cucumber');
const { Given, Then, When } = require('cucumber');

const home = client.page.home();

Given(/^A web browser is at automationpractice.com$/, async () => {
    await client.execute(`
        localStorage.clear();
        sessionStorage.clear();
      `).deleteCookies().refresh();
    await client.url('http://automationpractice.com/index.php');
});

When(/^I input Email '([^"]*)' on Newsletter$/, async(email) => {
    await home.inputEmail(email);
});

When(/^I click Submit button$/, async() => {
    await home.clickSubmitButton();
});

Then(/^I get notification$/, async () => {
    authentication.verifyNewsletterNotification();
});

When(/^I click Sign In button$/, async() => {
    await home.clickSignIn();
})

