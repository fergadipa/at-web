const { client } = require('nightwatch-cucumber');
const { When } = require('cucumber');

const authentication = client.page.authentication();

When(/^I input email '([^"]*)' and password '([^"]*)'$/, async(email,password) => {
    await authentication.inputEmail(email);
    await authentication.inputPassword(password);
})

When(/^I click Submit Login button$/, async() => {
    await authentication.clickSubmitLogin();
})

When(/^I input email '([^"]*)'$/, async(emailaccount) => {
    await authentication.inputEmailCreateAccount(emailaccount);
})

When(/^I click create an account button$/, async() => {
    await authentication.clickSubmitCreateAccount();
})
