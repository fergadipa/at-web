const { client } = require('nightwatch-cucumber');
const { Then } = require('cucumber');

const myAccount = client.page.my_account();

Then(/^I can successfully login as '([^"]*)'$/, async(nama) => {
    await myAccount.verificationName(nama);
})