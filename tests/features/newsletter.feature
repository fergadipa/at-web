Feature: Newsletter
   
   @newsletter
   Scenario Outline: As a customer I can get subscribe notification successfully
   Given A web browser is at automationpractice.com
   When I input Email '<email>' on Newsletter
   And I click Submit button
   Then I get notification

Examples:
| email |
| hello3@mailinator.com  | 
| hello4@mailinator.com  |
