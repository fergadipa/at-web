Feature: Login

   @login
   Scenario Outline: As a customer, I can login successfully
   Given A web browser is at automationpractice.com
   When I click Sign In button
   And I input email '<email>' and password '<password>'
   And I click Submit Login button
   Then I can successfully login as '<nama>'

   Examples:
   | email | password | nama |
   | nindyharisputri99@gmail.com  | 000999  | Nindy Haris Putri  |
   | fergadipab@gmail.com | test12345 | Ferga Dipa |