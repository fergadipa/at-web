Feature: CreateAccount

   @createaccount
   Scenario Outline: As a customer, I can create an account successfully
   Given A web browser is at automationpractice.com
   When I click Sign In button
   And I input email '<emailaccount>'
   And I click create an account button
   Then I see account creation form

   Examples:
   | emailaccount |
   | farhadfabregas13@gmail.com  |
   | farhadfabregas14@gmail.com |